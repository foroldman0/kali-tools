---
Title: sharpshooter
Homepage: https://github.com/mdsecactivebreach/SharpShooter
Repository: https://gitlab.com/kalilinux/packages/sharpshooter
Architectures: all
Version: 2.0+git20240315.f3235c5-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sharpshooter
 
  SharpShooter is a payload creation framework for the retrieval and execution of
  arbitrary CSharp source code. SharpShooter is capable of creating payloads in a
  variety of formats, including HTA, JS, VBS and WSF.
 
 **Installed size:** `538 KB`  
 **How to install:** `sudo apt install sharpshooter`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-jsmin
 {{< /spoiler >}}
 
 ##### sharpshooter
 
 
 ```
 root@kali:~# sharpshooter -h
 
        _____ __                    _____ __                __           
       / ___// /_  ____ __________ / ___// /_  ____  ____  / /____  _____
       \__ \/ __ \/ __ `/ ___/ __ \__ \/ __ \/ __ \/ __ \/ __/ _ \/ ___/
      ___/ / / / / /_/ / /  / /_/ /__/ / / / / /_/ / /_/ / /_/  __/ /    
     /____/_/ /_/\__,_/_/  / .___/____/_/ /_/\____/\____/\__/\___/_/     
                          /_/                                            
 
      Dominic Chell, @domchell, MDSec ActiveBreach, v2.0
     
 usage: sharpshooter [-h] [--stageless] [--dotnetver <ver>] [--com <com>]
                     [--awl <awl>] [--awlurl <awlurl>] [--payload <format>]
                     [--sandbox <types>] [--amsi <amsi>] [--delivery <type>]
                     [--rawscfile <path>] [--shellcode] [--scfile <path>]
                     [--refs <refs>] [--namespace <ns>] [--entrypoint <ep>]
                     [--web <web>] [--dns <dns>] [--output <output>]
                     [--smuggle] [--template <tpl>]
 
 options:
   -h, --help          show this help message and exit
   --stageless         Create a stageless payload
   --dotnetver <ver>   Target .NET Version: 2 or 4
   --com <com>         COM Staging Technique: outlook, shellbrowserwin, wmi, wscript, xslremote
   --awl <awl>         Application Whitelist Bypass Technique: wmic, regsvr32
   --awlurl <awlurl>   URL to retrieve XSL/SCT payload
   --payload <format>  Payload type: hta, js, jse, vbe, vbs, wsf, macro, slk
   --sandbox <types>   Anti-sandbox techniques: 
                       [1] Key to Domain (e.g. 1=CONTOSO)
                       [2] Ensure Domain Joined
                       [3] Check for Sandbox Artifacts
                       [4] Check for Bad MACs
                       [5] Check for Debugging
   --amsi <amsi>       Use amsi bypass technique: amsienable
   --delivery <type>   Delivery method: web, dns, both
   --rawscfile <path>  Path to raw shellcode file for stageless payloads
   --shellcode         Use built in shellcode execution
   --scfile <path>     Path to shellcode file as CSharp byte array
   --refs <refs>       References required to compile custom CSharp,
                       e.g. mscorlib.dll,System.Windows.Forms.dll
   --namespace <ns>    Namespace for custom CSharp,
                       e.g. Foo.bar
   --entrypoint <ep>   Method to execute,
                       e.g. Main
   --web <web>         URI for web delivery
   --dns <dns>         Domain for DNS delivery
   --output <output>   Name of output file (e.g. maldoc)
   --smuggle           Smuggle file inside HTML
   --template <tpl>    Name of template file (e.g. mcafee)
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
