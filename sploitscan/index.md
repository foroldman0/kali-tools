---
Title: sploitscan
Homepage: https://github.com/xaitax/SploitScan
Repository: https://salsa.debian.org/pkg-security-team/SploitScan
Architectures: all
Version: 0.5.0+git20240429.5243d70-1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sploitscan
 
  SploitScan is an efficient and easy-to-use command-line tool designed to
  consult CVE (Common Vulnerabilities and Exposures).
  Extremely important for professionals, as it allows them to implement
  measures that prevent the exploitation of discovered vulnerabilities.
   
  Tool is capable of exporting in a single run results for JSON and CSV
  formats, from the main databases of entities and organizations linked
  to cybersecurity:
     - MITER Corporation. non-profit organization that provides technical
       solutions to critical security issue
     - EPSS (Exploit Prediction Scoring System) is a system that aims to
       predict the probability of a specific vulnerability, helping organizations
       prioritize their mitigation activities.
     - Gather PoCs (Proof of Concepts) Collecting PoCs is a common activity
       among vulnerability researchers and security professionals, who reproduce
       the severity of vulnerabilities in a controlled environment, aiming to
       take preventive measures.
     - CISA Infrastructure Security Agency - responsible for keeping Known
       Exploited Vulnerabilities Catalog (KEV) updated.
     - Capable of interacting with the Patch Priority System, responsible for
       evaluating and assigning a patch priority rating based on several factors,
       including the availability of public exploits.
 
 **Installed size:** `67 KB`  
 **How to install:** `sudo apt install sploitscan`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-tabulate
 {{< /spoiler >}}
 
 ##### sploitscan
 
 A tool to fetch and display vulnerability information and public exploits for given CVE IDs.
 
 ```
 root@kali:~# sploitscan -h
 
 ███████╗██████╗ ██╗      ██████╗ ██╗████████╗███████╗ ██████╗ █████╗ ███╗   ██╗
 ██╔════╝██╔══██╗██║     ██╔═══██╗██║╚══██╔══╝██╔════╝██╔════╝██╔══██╗████╗  ██║
 ███████╗██████╔╝██║     ██║   ██║██║   ██║   ███████╗██║     ███████║██╔██╗ ██║
 ╚════██║██╔═══╝ ██║     ██║   ██║██║   ██║   ╚════██║██║     ██╔══██║██║╚██╗██║
 ███████║██║     ███████╗╚██████╔╝██║   ██║   ███████║╚██████╗██║  ██║██║ ╚████║
 ╚══════╝╚═╝     ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═══╝
 v0.5 / Alexander Hagenah / @xaitax / ah@primepage.de
 
 usage: sploitscan [-h] [-e {json,csv}] cve_ids [cve_ids ...]
 
 SploitScan: Fetch and display data from NVD and public exploits for given CVE
 IDs.
 
 positional arguments:
   cve_ids               Enter one or more CVE IDs to fetch data. Separate
                         multiple CVE IDs with spaces. Format for each ID: CVE-
                         YYYY-NNNNN (Example: CVE-2023-23397 CVE-2024-12345)
 
 options:
   -h, --help            show this help message and exit
   -e {json,csv}, --export {json,csv}
                         Optional: Export the results to a JSON or CSV file.
                         Specify the format: 'json' or 'csv'.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
