---
Title: mitm6
Homepage: https://github.com/dirkjanm/mitm6
Repository: https://gitlab.com/kalilinux/packages/mitm6
Architectures: all
Version: 0.3.0-0kali1
Metapackages: 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### mitm6
 
  mitm6 is a pentesting tool that exploits the default configuration of Windows
  to take over the default DNS server. It does this by replying to DHCPv6
  messages, providing victims with a link-local IPv6 address and setting the
  attackers host as default DNS server.
 
 **Installed size:** `40 KB`  
 **How to install:** `sudo apt install mitm6`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-netifaces
 * python3-scapy
 * python3-twisted
 {{< /spoiler >}}
 
 ##### mitm6
 
 
 ```
 root@kali:~# mitm6 -h
 usage: mitm6 [-h] [-i INTERFACE] [-l LOCALDOMAIN] [-4 ADDRESS] [-6 ADDRESS]
              [-m ADDRESS] [-a] [-r TARGET] [-v] [--debug] [-d DOMAIN]
              [-b DOMAIN] [-hw DOMAIN] [-hb DOMAIN] [--ignore-nofqdn]
 
 mitm6 - pwning IPv4 via IPv6
 For help or reporting issues, visit https://github.com/dirkjanm/mitm6
 
 options:
   -h, --help            show this help message and exit
   -i INTERFACE, --interface INTERFACE
                         Interface to use (default: autodetect)
   -l LOCALDOMAIN, --localdomain LOCALDOMAIN
                         Domain name to use as DNS search domain (default: use
                         first DNS domain)
   -4 ADDRESS, --ipv4 ADDRESS
                         IPv4 address to send packets from (default:
                         autodetect)
   -6 ADDRESS, --ipv6 ADDRESS
                         IPv6 link-local address to send packets from (default:
                         autodetect)
   -m ADDRESS, --mac ADDRESS
                         Custom mac address - probably breaks stuff (default:
                         mac of selected interface)
   -a, --no-ra           Do not advertise ourselves (useful for networks which
                         detect rogue Router Advertisements)
   -r TARGET, --relay TARGET
                         Authentication relay target, will be used as fake DNS
                         server hostname to trigger Kerberos auth
   -v, --verbose         Show verbose information
   --debug               Show debug information
 
 Filtering options:
   -d DOMAIN, --domain DOMAIN
                         Domain name to filter DNS queries on (Allowlist
                         principle, multiple can be specified.)
   -b DOMAIN, --blocklist DOMAIN, --blacklist DOMAIN
                         Domain name to filter DNS queries on (Blocklist
                         principle, multiple can be specified.)
   -hw DOMAIN, -ha DOMAIN, --host-allowlist DOMAIN, --host-whitelist DOMAIN
                         Hostname (FQDN) to filter DHCPv6 queries on (Allowlist
                         principle, multiple can be specified.)
   -hb DOMAIN, --host-blocklist DOMAIN, --host-blacklist DOMAIN
                         Hostname (FQDN) to filter DHCPv6 queries on (Blocklist
                         principle, multiple can be specified.)
   --ignore-nofqdn       Ignore DHCPv6 queries that do not contain the Fully
                         Qualified Domain Name (FQDN) option.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
