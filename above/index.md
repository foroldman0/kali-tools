---
Title: above
Homepage: https://github.com/cursedpkt/Above
Repository: https://gitlab.com/kalilinux/packages/above
Architectures: all
Version: 2.6-0kali1
Metapackages: kali-linux-everything kali-tools-sniffing-spoofing 
Icon: images/above-logo.svg
PackagesInfo: |
 ### above
 
  This package contains an invisible protocol sniffer for finding
  vulnerabilities in the network, designed for pentesters and security
  professionals.
   
  It is based entirely on network traffic analysis, so it does not make any
  noise on the air. Above allows pentesters to automate the process of finding
  vulnerabilities in network hardware. Discovery protocols, dynamic routing,
  FHRP, STP, LLMNR/NBT-NS, etc.
   
  The tool can also both listen to traffic on the interface and analyze already
  existing pcap files.
 
 **Installed size:** `77 KB`  
 **How to install:** `sudo apt install above`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-colorama
 * python3-scapy
 {{< /spoiler >}}
 
 ##### above
 
 
 ```
 root@kali:~# above -h
 
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░░░░▒▓▓▒░░░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░░░█████▓░░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░░███▒▒██▓░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░███▒░░▒██▓░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░███▒░░░░▒██▓░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░███▒░░█▓░░▒██▓░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░███▒░░███▓░░▒██▓░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░███▒░░█████▓░░▒██▓░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░███▒░░███░░██▓░░▒██▓░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░███▒░░███░░░░██▓░░▒██▓░░░░░░░░░░░░░
     ░░░░░░░░░░░░███▒░░███▓▒▒▒▒▓██▓░░▒██▓░░░░░░░░░░░░
     ░░████████████▒░░█████████████▓░░▒████████████░░
     ░░███░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░███░░
     ░░███░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░███░░
     ░░█████████░░░████████████████████░░▒█████████░░
     ░░░░░░▒███▒░░███░░░░░░░░░░░░░░░░███░░▒███░░░░░░░
     ░░░░░░███░░░███░░░░░░░░░░░░░░░░░░███░░▒███░░░░░░
     ░░░░░███░░░██████████████████████████░░▒███░░░░░
     ░░░░███░░░▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒░░▒███░░░░
     ░░░███▒░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒███░░░
     ░░░██████████████████████████████████████████░░░
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 
     Above: Invisible network protocol sniffer
     Designed for pentesters and security engineers
 
     For documentation and usage examples, visit: https://github.com/casterbyte/Above
 
     Author: Magama Bazarov, <caster@exploit.org>
     Pseudonym: Caster
     Version: 2.6
     Codename: Introvert
 
 usage: above [-h] [--interface INTERFACE] [--timer TIMER] [--output OUTPUT]
              [--input INPUT] [--passive-arp]
 
 options:
   -h, --help            show this help message and exit
   --interface INTERFACE
                         Interface for traffic listening
   --timer TIMER         Time in seconds to capture packets, if not set capture
                         runs indefinitely
   --output OUTPUT       File name where the traffic will be recorded
   --input INPUT         File name of the traffic dump
   --passive-arp         Passive ARP (Host Discovery)
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
