---
Title: juice-shop
Homepage: https://github.com/juice-shop/juice-shop
Repository: https://gitlab.com/kalilinux/packages/juice-shop
Architectures: amd64
Version: 15.2.1+node18-0kali3
Metapackages: kali-linux-labs 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### juice-shop
 
  This package contains a modern and sophisticated insecure web application! It
  can be used in security trainings, awareness demos, CTFs and as a guinea pig
  for security tools! Juice Shop encompasses vulnerabilities from the entire
  OWASP Top Ten along with many other security flaws found in real-world
  applications!
   
  WARNING: Do not upload it to your hosting provider's public html folder or any
  Internet facing servers, as they will be compromised.
 
 **Installed size:** `426.33 MB`  
 **How to install:** `sudo apt install juice-shop`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * libc6 
 * libgcc-s1 
 * libstdc++6 
 * lsof
 * nodejs 
 * npm
 * xdg-utils
 {{< /spoiler >}}
 
 ##### juice-shop
 
 
 ```
 root@kali:~# juice-shop -h
 [*] Please wait for the Juice-shop service to start.
 [*]
 [*] You might need to refresh your browser once it opens.
 [*]
 [*]  Web UI: http://127.0.0.1:42000
 
 ```
 
 - - -
 
 ##### juice-shop-stop
 
 
 ```
 root@kali:~# juice-shop-stop -h
 x juice-shop.service - juice-shop web application
      Loaded: loaded (/usr/lib/systemd/system/juice-shop.service; disabled; preset: disabled)
      Active: failed (Result: exit-code) since Wed 2024-05-22 05:47:14 EDT; 3s ago
    Duration: 1.665s
     Process: 644211 ExecStart=npm start (code=exited, status=1/FAILURE)
    Main PID: 644211 (code=exited, status=1/FAILURE)
         CPU: 2.002s
 
 May 22 05:47:14 kali npm[645269]:     at require (node:internal/modules/helpers:177:18)
 May 22 05:47:14 kali npm[645269]:     at Object.<anonymous> (/var/lib/juice-shop/node_modules/libxmljs2/index.js:4:18)
 May 22 05:47:14 kali npm[645269]:     at Module._compile (node:internal/modules/cjs/loader:1364:14)
 May 22 05:47:14 kali npm[645269]:     at Module._extensions..js (node:internal/modules/cjs/loader:1422:10) {
 May 22 05:47:14 kali npm[645269]:   code: 'ERR_DLOPEN_FAILED'
 May 22 05:47:14 kali npm[645269]: }
 May 22 05:47:14 kali npm[645269]: Node.js v18.20.1
 May 22 05:47:14 kali systemd[1]: juice-shop.service: Main process exited, code=exited, status=1/FAILURE
 May 22 05:47:14 kali systemd[1]: juice-shop.service: Failed with result 'exit-code'.
 May 22 05:47:14 kali systemd[1]: juice-shop.service: Consumed 2.002s CPU time.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
