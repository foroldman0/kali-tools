---
Title: joplin
Homepage: https://github.com/laurent22/joplin
Repository: https://gitlab.com/kalilinux/packages/joplin
Architectures: amd64 arm64
Version: 3.0.2+ds-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### joplin
 
  This package contains a free, open source note taking and to-do application,
  which can handle a large number of notes organised into notebooks. The notes
  are searchable, can be copied, tagged and modified either from the
  applications directly or from your own text editor. The notes are in Markdown
  format.
   
  Notes exported from Evernote via .enex files can be imported into Joplin,
  including the formatted content (which is converted to Markdown), resources
  (images, attachments, etc.) and complete metadata (geolocation, updated time,
  created time, etc.). Plain Markdown files can also be imported.
   
  The notes can be synchronised with various cloud services including Nextcloud,
  Dropbox, OneDrive, WebDAV or the file system (for example with a network
  directory). When synchronising the notes, notebooks, tags and other metadata
  are saved to plain text files which can be easily inspected, backed up and
  moved around.
 
 **Installed size:** `713.02 MB`  
 **How to install:** `sudo apt install joplin`  
 
 {{< spoiler "Dependencies:" >}}
 * libasound2 
 * libatk-bridge2.0-0 
 * libatk1.0-0 
 * libatspi2.0-0 
 * libc6 
 * libcairo2 
 * libcups2 
 * libdbus-1-3 
 * libdrm2 
 * libexpat1 
 * libgbm1 
 * libgcc-s1 
 * libglib2.0-0 
 * libgtk-3-0 
 * libnspr4 
 * libnss3 
 * libpango-1.0-0 
 * libstdc++6 
 * libx11-6 
 * libxcb1 
 * libxcomposite1 
 * libxdamage1 
 * libxext6
 * libxfixes3
 * libxkbcommon0 
 * libxrandr2
 * nodejs
 {{< /spoiler >}}
 
 ##### joplin
 
 
 
 - - -
 
 ### joplin-cli
 
  This package contains a free, open source note taking and to-do application,
  which can handle a large number of notes organised into notebooks. The notes
  are searchable, can be copied, tagged and modified either from the
  applications directly or from your own text editor. The notes are in Markdown
  format.
   
  Notes exported from Evernote via .enex files can be imported into Joplin,
  including the formatted content (which is converted to Markdown), resources
  (images, attachments, etc.) and complete metadata (geolocation, updated time,
  created time, etc.). Plain Markdown files can also be imported.
   
  The notes can be synchronised with various cloud services including Nextcloud,
  Dropbox, OneDrive, WebDAV or the file system (for example with a network
  directory). When synchronising the notes, notebooks, tags and other metadata
  are saved to plain text files which can be easily inspected, backed up and
  moved around.
   
  This package contains the Command Line Interface.
 
 **Installed size:** `1.50 GB`  
 **How to install:** `sudo apt install joplin-cli`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libcairo2 
 * libfreetype6 
 * libgcc-s1 
 * libgdk-pixbuf-2.0-0 
 * libgif7 
 * libglib2.0-0 
 * libharfbuzz0b 
 * libjpeg62-turbo 
 * libpango-1.0-0 
 * libpangocairo-1.0-0 
 * libpixman-1-0
 * libpng16-16 
 * librsvg2-2 
 * libsecret-1-0 
 * libstdc++6 
 * libuuid1 
 * nodejs
 * zlib1g 
 {{< /spoiler >}}
 
 ##### joplin-cli
 
 
 ```
 root@kali:~# joplin-cli -h
 Unknown flag: -h
 Type `joplin help` for usage information.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
