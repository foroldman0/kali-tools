---
Title: horst
Homepage: https://github.com/br101/horst
Repository: https://salsa.debian.org/debian/horst
Architectures: any
Version: 5.1-3
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### horst
 
  horst is a small, lightweight IEEE802.11 WLAN analyzer with a text
  interface. Its basic function is similar to tcpdump, Wireshark or
  Kismet, but it's much smaller and shows different, aggregated
  information which is not easily available from other tools. It is
  made for debugging wireless LANs with a focus on getting a quick
  overview instead of deep packet inspection and has special features
  for Ad-hoc (IBSS) mode and mesh networks. It can be useful to get a
  quick overview of what's going on all wireless LAN channels and to
  identify problems.
   
   * Shows signal (RSSI) values per station, something hard to get,
     especially in IBSS mode
   * Calculates channel utilization ("usage") by adding up the amount of
     time the packets actually occupy the medium
   * "Spectrum Analyzer" shows signal levels and usage per channel
   * Graphical packet history, with signal, packet type and physical rate
   * Shows all stations per ESSID and the live TSF per node as it is
     counting
   * Detects IBSS "splits" (same ESSID but different BSSID – this is/was
     a common driver problem on IBSS mode)
   * Statistics of packets/bytes per physical rate and per packet type
   * Has some support for mesh protocols (OLSR and batman)
   * Can filter specific packet types, operating modes, source addresses
     or BSSIDs
   * Client/server support for monitoring on remote nodes
   * Automatically adds and removes monitor interface
   
  horst is a Linux program and can be used on any wireless LAN interface
  which supports monitor mode.
 
 **Installed size:** `150 KB`  
 **How to install:** `sudo apt install horst`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libncurses6 
 * libnl-3-200 
 * libnl-genl-3-200 
 * libtinfo6 
 {{< /spoiler >}}
 
 ##### horst
 
 Highly Optimized Radio Scanning Tool
 
 ```
 root@kali:~# horst -h
 
 Usage: horst [options]
 
 General Options: Description (default value)
   -v		show version
   -h		Help
   -q		Quiet, no output
   -D		Show lots of debug output, no UI
   -a		Always add virtual monitor interface
   -c <file>	Config file (/etc/horst.conf)
   -C <chan>	Set initial channel
   -i <intf>	Interface name (wlan0)
   -t <sec>	Node timeout in seconds (60)
   -d <ms>	Display update interval in ms (100)
   -V view	Display view: history|essid|statistics|spectrum
   -b <bytes>	Receive buffer size in bytes (not set)
   -M[filename]	MAC address to host name mapping (/tmp/dhcp.leases)
 
 Feature Options:
   -s		(Poor mans) Spectrum analyzer mode
   -u		Upper channel limit
 
   -N		Allow network connection, server mode (off)
   -n <IP>	Connect to server with <IP>, client mode (off)
   -p <port>	Port number of server (4444)
 
   -o <filename>	Write packet info into 'filename'
 
   -X[filename]	Allow control socket on 'filename' (/tmp/horst)
   -x <command>	Send control command
 
 Filter Options:
  Filters are generally 'positive' or 'inclusive' which means you define
  what you want to see, and everything else is getting filtered out.
  If a filter is not set it is inactive and nothing is filtered.
  Most filter options can be specified multiple times and will be combined
   -e <MAC>	Source MAC addresses (xx:xx:xx:xx:xx:xx), up to 9 times
   -f <PKT_NAME>	Filter packet types, multiple
   -m <MODE>	Operating mode: AP|STA|ADH|PRB|WDS|UNKNOWN, multiple
   -B <MAC>	BSSID (xx:xx:xx:xx:xx:xx), only one
 
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
