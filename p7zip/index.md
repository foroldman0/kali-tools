---
archived: "true"
Title: p7zip
Homepage: https://7-zip.org/
Repository: https://salsa.debian.org/debian/p7zip
Architectures: all
Version: 16.02+transitional.1
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-linux-nethunter kali-tools-forensics kali-tools-respond 
Icon: images/p7zip-logo.svg
PackagesInfo: |
 ### p7zip
 
  This is a transitional package. It can be safely removed.
 
 **Installed size:** `8 KB`  
 **How to install:** `sudo apt install p7zip`  
 
 {{< spoiler "Dependencies:" >}}
 * 7zip 
 {{< /spoiler >}}
 
 
 - - -
 
 ### p7zip-full
 
  This is a transitional package. It can be safely removed.
 
 **Installed size:** `12 KB`  
 **How to install:** `sudo apt install p7zip-full`  
 
 {{< spoiler "Dependencies:" >}}
 * 7zip 
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
